import React, { Component } from "react"
import "./ProgressBar.css"

export default class ProgressBar extends Component {
  render() {
    const style = { width: `${this.getPercentage()}%` }
    const className = `ProgressBar ${this.getColor()}`

    return (
      <section className={className}>
        <span className="outer">
          <span style={style} className="inner">
            {this.getPercentage()}%
          </span>
        </span>
      </section>
    )
  }

  getPercentage() {
    return Math.ceil(this.getRatio() * 100)
  }

  getRatio() {
    const { puzzle } = this.props
    return (puzzle.pressedNumber / puzzle.totalNumber)
  }

  getColor() {
    const ratio = this.getRatio()
    if(ratio < 0.4) return "red"
    if(ratio < 0.6) return "orange"
    if(ratio < 0.8) return "yellow"
    return "green"
  }
}
