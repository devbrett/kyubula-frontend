import React, { Component } from 'react'
import "./Cell.css"

class Cell extends Component {
  render() {
    return (
      <div className={this.className()} onClick={this.onClick.bind(this)}>
        <div className="overlay">
          {this.props.cell.number}
        </div>
        <div className="effect" />
      </div>
    )
  }

  className() {
    const { available } = this.props
    const { pressed, number } = this.props.cell
    return `Cell number-${number} ${pressed ? "pressed" : ""} ${available ? "available" : ""}`
  }

  onClick(){
    const { cell, soundManager, position, available } = this.props
    if(!available) return

    soundManager.play(cell.number)
    this.props.addAction({ kind: "selection", position })
  }
}

export default Cell
