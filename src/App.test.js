import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import Game from "./state/game"



it('renders without crashing', () => {
  const div = document.createElement('div');

  const store = Game.create({
    chapters: [
      { puzzles: [{ size: 2, cells: [1, 1, 1, 1] }] }
    ]
  })

  ReactDOM.render(<App store={store} />, div);
});
