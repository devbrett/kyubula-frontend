import React, { Component } from 'react'
import { observer } from "mobx-react"
import Cell from './components/Cell'
import ProgressBar from './components/ProgressBar'
import './App.css'

class App extends Component {
  constructor() {
    super()
    this.state = { currentChapter: 0, currentPuzzle: 0, face: 0 }
    this.renderCell = this.renderCell.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.onClickRestart = this.onClickRestart.bind(this)
    this.onClickAcknowledge = this.onClickAcknowledge.bind(this)
  }

  get chapter() {
    return this.props.store.chapters[this.state.currentChapter]
  }

  get puzzle () {
    return this.chapter.puzzles[this.state.currentPuzzle]
  }

  get className () {
    const { complete, locked } = this.puzzle
    return `App ${complete ? "complete" : ""} ${locked ? "locked" : ""}`
  }

  render() {
    if (!this.puzzle) return this.renderComplete()

    const rows = this.puzzle.forEachCell(this.renderCell).map(this.renderRow)
    const cubeStyle = {} // { transform: `rotateY(${this.state.face * 90}deg)` }

    return (
      <div className={this.className}>
        <section className="CubeContainer">
          <div style={cubeStyle} className="Cube">
            <section className="Puzzle center Face two">
              {rows}
            </section>
          </div>
        </section>

        <header>
          <h1>Kyubula</h1>
        </header>

        <ProgressBar puzzle={this.puzzle} />
        <div className="score">{this.renderScore()}</div>
      </div>
    )
  }

  renderRow(cells, y) {
    return (<div key={y} className="Row">{cells}</div>)
  }

  renderCell(cell, position, available) {
    const key = `${position.x}-${position.y}`

    const props = {
      position, cell, available,
      soundManager: this.props.soundManager,
      addAction: this.puzzle.addAction
    }

    return <Cell key={key} {...props} />
  }

  renderScore () {
    if(this.puzzle.complete) {
      return <button onClick={this.onClickAcknowledge}>COMPLETE! Continue...</button>
    }

    if(this.puzzle.locked) {
      return <button onClick={this.onClickRestart}>LOCKED! Restart...</button>
    }

    return <button onClick={this.onClickRestart}>Restart</button>
  }

  renderComplete() {
    return <h1>
      Wow! You completed all the beginner levels! More devilish puzzles are on their way. ;)
    </h1>
  }

  onClickRestart () {
    this.puzzle.restart()
  }

  onClickAcknowledge () {
    this.setState(Object.assign({}, { currentPuzzle: this.state.currentPuzzle + 1 }))
  }
}

export default observer(App)
