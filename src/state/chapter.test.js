import Chapter from "./chapter"

describe("Chapter", () => {
  it("has default attributes", () => {
    const chapter = Chapter.create({})
    expect(chapter.puzzles.toJSON()).toEqual([])
  })
})
