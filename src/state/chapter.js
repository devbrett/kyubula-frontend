import { types } from "mobx-state-tree"
import { isArray } from "lodash"
import Puzzle from "./puzzle"

export default types.model("Chapter", {

  // Attributes

  puzzles: types.optional(types.array(Puzzle), [])

}).preProcessSnapshot(snapshot => {

  if(isArray(snapshot)) { return { puzzles: snapshot } }
  return snapshot

})
