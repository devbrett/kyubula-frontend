import { types } from "mobx-state-tree"
import Chapter from "./chapter"

// Game

export default types.model("Game", {

  // Attributes

  chapters: types.array(Chapter),

}).actions(self => ({

  // Actions

  addPuzzle(puzzle) {
    self.puzzle = puzzle
  },

}))
