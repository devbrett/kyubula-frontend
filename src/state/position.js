import { types } from "mobx-state-tree"
import { isArray } from "lodash"

// Position

export default types.model("Position", {

  x: types.number,
  y: types.number

}).preProcessSnapshot(snapshot => {

  if(isArray(snapshot)) return { x: snapshot[0], y: snapshot[1] }
  return snapshot

}).views(self => ({

  distanceBetween(other) {
    if (!other) return null

    const horizontalDifference = Math.abs(other.x - self.x)
    const verticalDifference = Math.abs(other.y - self.y)

    return Math.max(horizontalDifference, verticalDifference)
  },

}))
