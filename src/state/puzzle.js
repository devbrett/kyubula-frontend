import { types } from "mobx-state-tree"
import { flatten, times, compact } from "lodash"

import Action from "./action"
import Cell from "./cell"
import Position from "./position"

// Puzzle

export default types.model("Puzzle", {

  size    : types.optional(types.number, 0),
  cells   : types.optional(types.array(Cell), []),
  actions : types.optional(types.array(Action), [])

}).preProcessSnapshot(snapshot => {

  if(snapshot === undefined) return
  return Object.assign(snapshot, { cells: flatten(snapshot.cells) })

}).views(self => ({

  get availableCells() {
    return this.forEachAvailableCell((cell, _coords) => cell)
  },

  get availablePositions() {
    return this.forEachAvailableCell((_cell, position) => position)
  },

  get cellsWithPositions() {
    return this.forEachCell((cell, position, available) => [cell, position, available])
  },

  forEachAvailableCell(callback) {
    return compact(flatten(this.forEachCell(
      (cell, position, available) => {
        if(available) return callback(cell, position)
        return null
      }
    )))
  },

  forEachCell(callback) {
    return times(this.size, x => times(this.size, y => {
      const position = Position.create({ x, y })
      const available = this.isCellAvailable(position)
      return callback(this.getAt(position), position, available)
    }))
  },

  get totalNumber() {
    return this.size * this.size
  },

  get pressedNumber() {
    return this.cells.filter(c => c.pressed).length
  },

  get locked() {
    return this.availableCells.length === 0
  },

  get complete() {
    if(this.actions.length === this.totalNumber) return true
    return false
  },

  isCellAvailable(position) {
    const cell = this.getAt(position)
    const lastSelection = this.lastSelection()

    if (cell.pressed) return false
    if (!lastSelection || !lastSelection.position) return true

    const distance = lastSelection.position.distanceBetween(position)
    const selectedCell = this.getAt(lastSelection.position)
    if (distance !== selectedCell.number) return false

    return true
  },

  toString() {
    return this.cells.map(c => c.toString()).join("\n")
  },

  getAt(position) {
    return this.cells[position.y*this.size + position.x]
  },

  lastSelection() {
    return this.actions.reverse().find(
      a => a.kind === "selection"
    ) || null
  },

})).actions(self => ({

  addAction(action) {
    self.actions.push(action)

    switch(action.kind) {
      case "selection":
        self.getAt(action.position).pressed = true
      break;

      default: throw new Error("Unmatched action kind")
    }
  },

  restart() {
    self.actions = []
    self.cells.map(c => c.pressed = false)
  }

}))
