import Action from "./action"

describe("Action", () => {
  it("has a kind attribute that defaults to 'selection'", () => {
    const action = Action.create({})
    expect(action.toJSON()).toEqual({ kind: "selection", position: null })
  })
})
