import Position from "./position"

describe("Position", () => {
  it("represents a x, y position", () => {
    const position = Position.create([123,456])

    expect(position.x).toEqual(123)
    expect(position.y).toEqual(456)
  })

  describe(".distanceBetween", () => {
    it("returns null when given no input", () => {
      const p1 = Position.create({ x: 1, y: 1 })
      const distance = p1.distanceBetween(null)

      expect(distance).toEqual(null)
    })

    it("calculates the distance between points", () => {
      const p1 = Position.create({ x: 1, y: 1 })
      const p2 = Position.create({ x: 3, y: 3 })
      const distance = p1.distanceBetween(p2)

      expect(distance).toEqual(2)
    })
  })
})
