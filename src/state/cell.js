import { types } from "mobx-state-tree"
import { isInteger } from "lodash"

export default types.model("Cell", {

  number: types.maybe(types.number),
  pressed: types.optional(types.boolean, false)

}).preProcessSnapshot(snapshot => {

  if(isInteger(snapshot)) { return { number: snapshot } }
  return snapshot

}).views(self => ({

  toString() {
    return `[${self.number}${self.selected ? "X" : "-"}]`
  },

}))
