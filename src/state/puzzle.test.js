import Puzzle from "./puzzle"
import Position from "./position"
import Action from "./action"
import Cell from "./cell"

describe("Puzzle", () => {
  describe("initialization", () => {
    it("can be created from no input", () => {
      const puzzle = Puzzle.create()
      expect(puzzle.cells.length).toEqual(0)
    })
  })

  it("has default attributes", () => {
    const puzzle = Puzzle.create()
    expect(puzzle.toJSON()).toEqual({ size: 0, cells: [], actions: [] })
  })

  it("accepts reasonable input", () => {
    const puzzle = Puzzle.create({ size: 1, cells: [9] })

    expect(puzzle.toJSON()).toEqual({
      size: 1, cells: [{ number: 9, pressed: false }], actions: []
    })
  })

  describe("availableCells", () => {
    it("filters out pressed cells", () => {
      const puzzle = Puzzle.create({
        size: 2,
        cells: [1, 2, 3, { number: 4, pressed: true }]
      })

      expect(puzzle.availableCells.length).toEqual(3)

      expect(puzzle.availableCells.map(
        cell => cell.number
      )).toEqual([1, 3, 2])
    })

    it("filters out cells the wrong distance from the selected cell", () => {
      const puzzle = Puzzle.create({ size: 3, cells: [1, 2, 3, 4, 5, 6, 7, 8, 9] })
      expect(puzzle.availableCells.length).toEqual(9)

      const action = Action.create({ kind: "selection", position: { x: 0, y: 0 } })
      puzzle.addAction(action)

      expect(puzzle.availableCells.length).toEqual(3)
      expect(
        puzzle.availableCells.map(c => c.number)
      ).toEqual([4, 2, 5])
    })
  })

  describe("availablePositions", () => {
    it("filters out pressed cells", () => {
      const puzzle = Puzzle.create({
        size: 2,
        cells: [1, 2, 3, { number: 4, pressed: true }]
      })

      const positions = puzzle.availablePositions.map(
        position => ([position.x, position.y])
      )

      expect(positions).toEqual([[0, 0], [0, 1], [1, 0]])
    })
  })

  describe("cellsWithPositions", () => {
    it("returns an array of cells with their positions", () => {
      const puzzle = Puzzle.create({ size: 1, cells: [1] })
      const pairs = puzzle.cellsWithPositions

      expect(pairs).toEqual([[[
        Cell.create({ number: 1, pressed: false }),
        Position.create({ x: 0, y: 0 }),
        true
      ]]])
    })
  })

  describe("getAt", () => {
    it("returns the cell at that position", () => {
      const puzzle = Puzzle.create({ size: 3, cells: [
        11, 21, 31,
        12, 22, 32,
        13, 23, 33
      ] })

      expect(numberAt(puzzle, 0, 0)).toEqual(11)
      expect(numberAt(puzzle, 1, 1)).toEqual(22)
      expect(numberAt(puzzle, 2, 2)).toEqual(33)
    })
  })
})

const numberAt = (puzzle, x, y) => {
  const position = Position.create({ x, y })
  const cell = puzzle.getAt(position)
  return cell.number
}
