import Cell from "./cell"

describe("Cell", () => {
  it("has default attributes", () => {
    const cell = Cell.create({})

    expect(cell.number).toEqual(null)
    expect(cell.pressed).toEqual(false)
  })

  it("can be created via a shorthand with a single integer", () => {
    const cell = Cell.create(123)
    expect(cell.number).toEqual(123)
  })
})
