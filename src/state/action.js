import { types } from "mobx-state-tree"
import Position from "./position"

export default types.model("Action", {
  kind: types.optional(types.string, "selection"),
  position: types.maybe(Position)
}).actions(self => ({
}))
