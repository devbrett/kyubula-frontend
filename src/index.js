import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Game from "./state/game"
import chapters from "./chapters/index"
import SoundManager from "./sound/manager"

const soundManager = new SoundManager()
const store = Game.create({ chapters })
const props = { store, soundManager }

ReactDOM.render(
  <App {...props} />,
  document.getElementById('root')
);

registerServiceWorker();
