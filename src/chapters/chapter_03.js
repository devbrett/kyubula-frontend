const levels = [{
  size: 5, seed: "031", cells: [
     1, 2, 2, 1, 3,
     2, 3, 1, 3, 4,
     2, 2, 2, 1, 2,
     3, 1, 2, 1, 3,
     3, 3, 2, 1, 3,
  ]
}, {
  size: 5, seed: "032", cells: [
     3, 3, 4, 1, 4,
     3, 3, 2, 1, 1,
     1, 2, 2, 2, 2,
     4, 1, 1, 3, 3,
     4, 1, 4, 3, 3,
  ]
}, {
  size: 5, seed: "033", cells: [
     3, 3, 1, 3, 1,
     1, 3, 1, 1, 4,
     2, 1, 1, 1, 4,
     3, 2, 2, 1, 3,
     4, 1, 1, 1, 1,
  ]
}, {
  size: 5, seed: "034", cells: [
     1, 1, 3, 3, 4,
     2, 1, 2, 1, 3,
     2, 2, 1, 3, 1,
     1, 2, 3, 1, 1,
     4, 1, 2, 1, 1,
  ]
}, {
  size: 5, seed: "035", cells: [
     3, 1, 2, 2, 2,
     2, 3, 1, 1, 1,
     3, 3, 1, 1, 2,
     1, 1, 3, 2, 4,
     2, 1, 1, 1, 2,
  ]
}]

export default levels
