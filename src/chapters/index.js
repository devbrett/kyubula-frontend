import chapter1 from "./chapter_01"
import chapter2 from "./chapter_02"
import chapter3 from "./chapter_03"

const chapters = [
  chapter1,
  chapter2,
  chapter3
]

export default chapters
