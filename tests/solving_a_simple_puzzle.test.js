import Puzzle from "../src/state/puzzle"
import { step } from "./test_helpers"

it("models the solving of a simple puzzle", () => {
  step("Create a puzzle")
  const puzzle = Puzzle.create(puzzleFixture)

  step("No actions are registered yet")
  expect(puzzle.actions.toJSON()).toEqual([])

  step("All cells are available to select")
  expect(puzzle.availableCells.length).toEqual(9)

  step("I select the bottom right cell")
  makeMove(puzzle, { x: 2, y: 2 })

  step("Only the 3 cells touching it are available (it's a 1)")
  expect(availablePositions(puzzle)).toEqual([[1,1], [1,2], [2,1]])

  step("I select the middle right cell")
  makeMove(puzzle, { x: 2, y: 1 })

  step("Only the 3 cells in the left column are available (it's a 2)")
  expect(availablePositions(puzzle)).toEqual([[0,0], [0,1], [0,2]])

  step("I make all my moves except the last one")
  makeMoves(puzzle, [
    { x: 1, y: 2 }, { x: 2, y: 0 }, { x: 1, y: 1 }, { x: 1, y: 0 },
    { x: 0, y: 2 }, { x: 0, y: 1 }
  ])

  step("Only the final cell is available")
  // expect(availablePositions(puzzle)).toEqual([[0,0]])
})

const makeMove = (puzzle, position) => puzzle.addAction({ kind: "selection", position })
const makeMoves = (puzzle, positions) => positions.map(p => makeMove(puzzle, p))

const availablePositions = (puzzle) => puzzle.availablePositions.map(
  position => [position.x, position.y]
)

const convert = (cells2dArray) => ({
  rows: cells2dArray.map(cells => ({ cells: cells.map(number => ({ number })) }))
})

// Solution: [{2, 2}, {2, 1}, {1, 2}, {2, 0}, {1, 1}, {1, 0}, {0, 2}, {0, 1}, {0, 0}]
const puzzleFixture = {
  size: 3,
  cells: [
    1,1,1,
    2,1,2,
    1,1,1
  ]
}
